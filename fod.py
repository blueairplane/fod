#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Fountain of Dreams launcher

Fountain of Dreams is an old DOS post-apocalyptic RPG. It modified all
the game files to save -- to start a new game you had to reinstall. This
program sets up a nice interface to create new games & launch games in
progress.

Individual games are stored in their own directory (~/.fod/<GAME_NAME>
by default).
Master files are located by default in /usr/local/share/games/fod/
-- they should be read-only for safety, but this program will not
attempt to modify them.
"""

from collections import namedtuple
from pathlib import Path
import shutil
import subprocess
import sys

from PyQt5.QtWidgets import (QApplication, QWidget, QHBoxLayout, QVBoxLayout,
    QPushButton, QComboBox, QInputDialog, QMessageBox)


Config = namedtuple('Config', ('master_dir', 'saves_dir'),
    defaults=('/usr/local/share/games/fod/', '~/.fod/'))


class MainWindow(QWidget):
    """The main window of the program.
    
    Has "Start Game" & "Delete Game" buttons and a combo box to select
    which game to act upon. The combo box includes a default "Create New
    Game" option that, if selected, will change the buttons to a single
    "Start New Game" button.
    """
    
    def __init__(self, config=Config()):
        """Create a new program window."""
        super().__init__()
        self.master_dir, self.saves_dir = (Path(d).expanduser()
            for d in config)
        games = self.load_games()
        
        self.NEW_GAME_TEXT = '<Create new game>'
        
        # Widgets
        new_button = QPushButton('Create new game')
        new_button.clicked.connect(self.new_game)
        
        start_button = QPushButton('Start selected game')
        start_button.clicked.connect(self.start_game)
        
        delete_button = QPushButton('Delete selected game')
        delete_button.clicked.connect(self.delete_game)
        
        self.games_list = QComboBox()
        self.games_list.addItem(self.NEW_GAME_TEXT)
        if games:
            self.games_list.addItems(games)
            self.games_list.setCurrentIndex(1)
        self.games_list.activated[str].connect(self.switch_buttons)
        
        # Layout
        self.new = QHBoxLayout()
        self.new.addWidget(new_button)
        
        self.buttons = QHBoxLayout()
        self.buttons.addWidget(start_button)
        self.buttons.addWidget(delete_button)
        
        self.combo = QHBoxLayout()
        self.combo.addWidget(self.games_list)
        
        self.vbox = QVBoxLayout(self)
        self.vbox.addLayout(self.new)
        self.vbox.addLayout(self.buttons)
        self.vbox.addLayout(self.combo)
        
        self.setWindowTitle('Fountain of Dreams')
        self.show()
        self.setMaximumSize(self.size())
        self.setMinimumSize(self.size())
        
        self.switch_buttons()
        
    @property
    def selected_game(self):
        """Return the name of the selected game."""
        return self.games_list.currentText()
        
    def load_games(self):
        """Find saved game directories and return a list of their
        names."""
        if not self.saves_dir.exists():
            self.saves_dir.mkdir()
        return [d.name for d in self.saves_dir.iterdir() if d.is_dir()]
        
    def new_game(self):
        """Create new game directory."""
        name, ok = QInputDialog.getText(self, 'New Game',
            'Enter name of new game:')
        if ok:
            new_dir = self.saves_dir / name
            if new_dir.exists():
                alert = QMessageBox()
                alert.setText('Cannot create game: {}'.format(name))
                alert.setInformativeText('A game with that name already '
                                         'exists.')
                alert.setWindowTitle(' ')
                alert.exec_()
            else:
                new_dir.mkdir()
                for f in self.master_dir.iterdir():
                    shutil.copy(str(f), str(new_dir))
                user = Path.home().owner()
                for f in new_dir.iterdir():
                    shutil.chown(str(f), user, user)
                    f.chmod(0o644)
                self.games_list.addItem(name)
                self.games_list.setCurrentText(name)
                self.switch_buttons()
        
    def start_game(self):
        """Start the selected game."""
        self.hide()
        subprocess.run(['dosbox', '-exit',
            str(self.saves_dir / self.selected_game / 'FOD.EXE')])
        self.show()        
        
    def delete_game(self):
        """Delete the selected game."""
        for f in (self.saves_dir / self.selected_game).iterdir():
            f.unlink()
        (self.saves_dir / self.selected_game).rmdir()
        self.games_list.removeItem(self.games_list.currentIndex())
        self.switch_buttons()
        
    def switch_buttons(self):
        """Change layout, if needed."""
        if self.selected_game == self.NEW_GAME_TEXT:
            show, hide = self.new, self.buttons
        else:
            show, hide = self.buttons, self.new
        show_children = (show.itemAt(n).widget() for n in range(show.count()))
        for child in show_children:
            child.show()
        hide_children = (hide.itemAt(n).widget() for n in range(hide.count()))
        for child in hide_children:
            child.hide()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    main = MainWindow()
    sys.exit(app.exec_())
